---
layout: page
title: blender
published: true
---
## edit mode

    h = hide (alt+h = unhide)
    L = select connected
    gg = vertex slide
    ctrl + r = loops
    shift + d = duplicate
    p = separate by
    e = extrude
    i = inset
    ctrl + m = merge
    s + y + 0 = scale by y to 0
    select two vertices + j = cut
    z = wireframe toggle
    ctrl + i = invert (selection)
    ctrl + + = increase selection?, + - = decrease?
    alt + shift + s = smooth
    alp + p = triangulate?
    alt + j = ?
    shift + r = repeat last step
    ctrl + b = bevel
    ctrl + m = mirror
    r + yy = rotate around active edge?
    ctrl + tab (+ 1,2,3) = mesh select mode (edge, vertex, face)
    
## object mode

    shift + a = add stuff
    ctrl + alt + shift + c = set origin ...
    
## bezier

    ctrl + c = close curve
    
## addons

- booltool (bundled)
- floorgen (bundled?)
- f2 (bundled?)
- node wrangler (bundled)
- Dynamic Sky (free)
- Animation Nodes (free) < amazing!
- Sverchok (free)
- hard ops (commercial)

## Shortcut infographics

[infographics](https://tplinnovator.files.wordpress.com/2015/09/blender3d-shortcuts-infographic.jpg)  