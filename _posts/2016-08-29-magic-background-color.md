---
published: true
layout: post
date: '2016-08-29 19:36 +0200'
title: Magic background color
---
<svg width="720" height="200">
  <rect width="720" height="200" rx="5" ry="5" 
  style="fill:#315666" />
</svg> 
315666 > Teal Blue

<svg width="720" height="200">
  <rect width="720" height="200" rx="5" ry="5" 
  style="fill:#2842B3" />
</svg> 
2842B3 > Royal Blue

<svg width="720" height="200">
  <rect width="720" height="200" rx="5" ry="5" 
  style="fill:#313E44" />
</svg> 
313E44 > Big stone
