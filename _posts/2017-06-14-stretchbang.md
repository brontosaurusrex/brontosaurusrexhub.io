---
published: true
layout: post
date: '2017-07-02 21:00 +0200'
title: stretchbang
---
## 2d
[![stretchbangGreen2_1920x1200.th.png](https://cdn.scrot.moe/images/2017/06/18/stretchbangGreen2_1920x1200.th.png)](https://cdn.scrot.moe/images/2017/06/18/stretchbangGreen2_1920x1200.png)
[![stretchbangBlau.th.png](https://cdn.scrot.moe/images/2017/06/14/stretchbangBlau.th.png)](https://scrot.moe/image/14Ncv) [![stretcbangGreen.th.png](https://cdn.scrot.moe/images/2017/06/14/stretcbangGreen.th.png)](https://scrot.moe/image/14TCY) [![awsomeFont.th.png](https://cdn.scrot.moe/images/2017/06/14/awsomeFont.th.png)](https://scrot.moe/image/14Uhd)

svg should be in [git](https://github.com/brontosaurusrex/postbang/blob/master/images/svg/stretchbangOstrichFont.svg).

## 3d
[![strecth4_kritaPost.th.png](https://cdn.scrot.moe/images/2017/07/01/strecth4_kritaPost.th.png)](https://cdn.scrot.moe/images/2017/07/01/strecth4_kritaPost.png)
[![strecth4post2dsBW1600w.th.png](https://cdn.scrot.moe/images/2017/07/02/strecth4post2dsBW1600w.th.png)](https://cdn.scrot.moe/images/2017/07/02/strecth4post2dsBW1600w.png)
[![stretch.th.png](https://cdn.scrot.moe/images/2017/07/01/stretch.th.png)](https://cdn.scrot.moe/images/2017/07/01/stretch.png)

### material node using new principled uber shader
![scrot27611-fs8.png]({{site.baseurl}}/media/scrot27611-fs8.png)
